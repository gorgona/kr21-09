<?php

use yii\db\Schema;
use yii\db\Migration;

class m151026_191203_student extends Migration
{
    public function up()
    {
        $this->createTable('student', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'teacher_id' => Schema::TYPE_INTEGER,
        ]);

        $this->addForeignKey('fkTeacherStudent', 'Student', 'teacher_id', 'teacher', 'id', 'CASCADE', 'CASCADE');
        $this->insert('student', [
            'name' => 'vasya',
            'teacher_id' => '1',
        ]);

        $this->insert('student', [
            'name' => 'masha',
            'teacher_id' => '1',
        ]);
        $this->insert('student', [
            'name' => 'kolya',
            'teacher_id' => '1',
        ]);
    }

    public function down()
    {
        echo "m151026_191203_student cannot be reverted.\n";
        $this->dropForeignTable('fkTeacherStudent');
        $this->dropTable('student');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
