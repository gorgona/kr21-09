<?php

use yii\db\Schema;
use yii\db\Migration;


//������� ������� human ��� ������ ��������, �������� ������� id, name, age, description
//������� ������ Human ���������� � ������� human, ��������� ������� ���� ������� � �������� age = 22
//������� ������� ������ human


class m151027_202633_human extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        $this->createTable('human',[
            'id'=>Schema::TYPE_PK,
            'name'=>Schema::TYPE_STRING,
            'age'=>Schema::TYPE_INTEGER,
            'description'=>Schema::TYPE_STRING,
        ],$tableOptions);
        $this->insert('human',[
            'name'=>'Vasya',
            'age'=>'22',
            'description'=>'his color is green',
        ]);
    }

    public function down()
    {
        $this->dropTable('human');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
