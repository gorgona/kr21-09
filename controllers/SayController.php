<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

/**
 * Created by PhpStorm.
 * User: �����
 * Date: 25.10.2015
 * Time: 21:02
 */
class SayController extends Controller
{
    public function actionSay($target = 'world ')
    {
        return $this->render('say', ['target' => $target]);
    }
}