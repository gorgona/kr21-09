<?php

namespace app\controllers;

use Yii;
use app\models\human;
use app\models\teacher;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
      //  $teachers=Teacher::findAll([
      //      'id'=>1;
      //  ]);
      //  $teacher = Teacher::find()->where(['id'=>1])->asArray()->one();
      //  $teacher = Teacher::find()->where(['id'=>1])->orderby('first_name')->asArray()->all();
// TEACHER::deleteAll(['id'=>1]);
       // $teacher->delete();
       // $t=Teacher::findOne('1');
       // $students=$t->student;
       // var_dump($students);

       // $t=new Teacher();
       // $t->first_name='Vasya';
       // $t->last_name='Petrov';
       // $t->birth_date='1990-08-01 12:15:00';
       // $t->rating=3.5;
       // if($t->validate()) {
       //     $t->save();
       // }
       // $t->delete(); // ��������� ������ ������ � �������


      //  $teacher = Teacher::find()->orderby('first_name')->asArray()->all();

        //$t->setAttribute('first_name','hellow');
      //  $t->setAttributes([
      //      'first_name'=>'sss',
      //      'last_name'=>'ddd',
     //       'birth_date'=>'2012-09-10',
     //       'rating'=>3.5,
     //   ]);

       // echo '</pre>';
       // var_dump($teacher);
        //die('test');


        $t=new Human();
        $t->name='Petr';
        $t->age='25';
        $t->description='new';
        if($t->validate()){
            $t->save();
        }
      $human=Human::find()->where(['age'=>'22'])->orderby('name')->asArray()->all();
        echo '</pre>';
        var_dump($human);
        die('test');

        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
