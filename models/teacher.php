<?php
namespace app\models;

use yii\db\ActiveRecord;

class Teacher extends ActiveRecord{
    public static function tableName(){
        return 'teacher';
    }
    public function rules()
    {
        return [
            ['first_name','required'],
            ['last_name','reguired'],
            ['first_name','string','min'=>'5','max'=>'10'],
            [['rating','birth_date'],'required']
        ];
    }
}