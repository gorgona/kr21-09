<?php
/**
 * Created by PhpStorm.
 * User: spalah
 * Date: 26.10.2015
 * Time: 21:23
 */
namespace app\models;
use Yii\db\ActiveRecord;

class Student extends ActiveRecord{
    public static function tableName(){
        return 'students';
    }
    public function rules()
    {
        return [
            ['first_name','required'],
            ['last_name','reguired'],
            ['first_name','string','min'=>'5','max'=>'10'],
            [['rating','birth_date'],'required']
        ];
    }
    public function getStudent(){
        $this->hasMany(Student::classNAme(),['teacher_id'=>'id']);
    }
}