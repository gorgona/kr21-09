<?php
/**
 * Created by PhpStorm.
 * User: �����
 * Date: 27.10.2015
 * Time: 22:51
 */
//�������� ������� ������� ��������� ��� ����� name (string min length 3 max - 20), age (integer), description (safe)

namespace app\models;

use yii\db\ActiveRecord;

class Human extends ActiveRecord{
     public static function tableName()
    {
        return 'human';
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string','min' => 3, 'max' => 20],
            ['age', 'integer'],
            ['description', 'safe']
        ];
    }

    public function getHuman()
    {
        $this->hasMany(Human::className(), ['age' => '22']);
    }
}